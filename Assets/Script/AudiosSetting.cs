using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AudiosSetting : Singleton<AudiosSetting>
{
    public Slider _musicSlider, _sfxSlider;

    public void ToggleMusic()
    {
        AudiosManager.Instance.PlaySound("Button");
        AudiosManager.Instance.ToggleMusic();
    }
    public void ToggleSFX()
    {
        AudiosManager.Instance.PlaySound("Button");
        AudiosManager.Instance.ToggleSFX();
    }
    public void MusicVolume()
    {
        AudiosManager.Instance.MusicVolume(_musicSlider.value);
    }
    public void SFXVolume()
    {
        AudiosManager.Instance.SFXVolume(_sfxSlider.value);
    }
}
