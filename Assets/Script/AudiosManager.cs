using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudiosManager : Singleton<AudiosManager>
{
    public Sound[] Music, Sound;
    public AudioSource BGM, SFX;

    public void PlayMusic(string name)
    {
        Sound s = Array.Find(Music, x => x.name == name);

        if (s == null)
        {
            Debug.LogWarning("Music Not Found");
        }
        else
        {
            BGM.clip = s.clip;
            BGM.Play();
        }
    }

    public void PlaySound(string name)
    {
        Sound s = Array.Find(Sound, x => x.name == name);

        if (s == null)
        {
            Debug.LogWarning("Sound Not Found");
        }
        else
        {
            SFX.PlayOneShot(s.clip);
        }
    }
    
    public void ToggleMusic()
    {
        BGM.mute = !BGM.mute;
    }
    public void ToggleSFX()
    {
        SFX.mute = !SFX.mute;
    }
    
    public void MusicVolume(float value)
    {
        BGM.volume = value;
    }
    public void SFXVolume(float value)
    {
        SFX.volume = value;
    }
}
