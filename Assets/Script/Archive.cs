using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Archive : Singleton<Archive>
{
    public LevelStats[] levelsStats;
    
    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode loadSceneMode)
    {
        if (scene.buildIndex == 0)
        {
            Time.timeScale = 1;
            AudiosManager.Instance.PlayMusic("Menu");
        }
        if (scene.buildIndex == 1)
        {
            AudiosManager.Instance.PlayMusic("Level1");
        }
    }
}
