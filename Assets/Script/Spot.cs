using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public enum SpotType
{
    Real,
    Mirror
}

public class Spot : MonoBehaviour
{
    public SpotType Type;
    
    public bool IsHasBeenSpot = false;
    public void HitTheSpot(GameObject targetSpot)
    {
        switch (Type)
        {
            case SpotType.Real:
                HitRealSpot();
                break;
            case SpotType.Mirror:
                HitMirrorSpot();
                break;
        }
    }
    private void HitRealSpot()
    {
        IsHasBeenSpot = true;
        
        GameObject Mirror = GameObject.Find(gameObject.name + "_M");
        
        GameObject CanvasRef = GameObject.Find("Canvas");
        
        GameObject CircleRef = Instantiate(GameManage.Instance.CorrectSpot, gameObject.transform.position, quaternion.identity);
        GameObject MirrorRef = Instantiate(GameManage.Instance.CorrectSpot, Mirror.transform.position, quaternion.identity);
        if (CanvasRef != null)
        {
            CircleRef.transform.SetParent(CanvasRef.transform);
            MirrorRef.transform.SetParent(CanvasRef.transform);

            CircleRef.transform.localScale = new Vector3(0.7f, 0.7f, 0.7f);
            MirrorRef.transform.localScale = new Vector3(0.7f, 0.7f, 0.7f);
        }
        
        //targetSpot.SetActive(!targetSpot.activeSelf);
        Debug.LogWarning("Hit: " + gameObject.name);
        
        GameManage.Instance.SetScore();
    }

    private void HitMirrorSpot()
    {
        IsHasBeenSpot = true;
        
        string spotName = gameObject.name.Replace("_M", "");
        GameObject spotObject = GameObject.Find(spotName);
        
        GameObject CanvasRef = GameObject.Find("Canvas");
        
        GameObject MirrorRef = Instantiate(GameManage.Instance.CorrectSpot, gameObject.transform.position, quaternion.identity);
        GameObject CircleRef = Instantiate(GameManage.Instance.CorrectSpot, spotObject.transform.position, quaternion.identity);
        if (CanvasRef != null)
        {
            MirrorRef.transform.SetParent(CanvasRef.transform);
            CircleRef.transform.SetParent(CanvasRef.transform);

            MirrorRef.transform.localScale = new Vector3(0.7f, 0.7f, 0.7f);
            CircleRef.transform.localScale = new Vector3(0.7f, 0.7f, 0.7f);
        }
        Debug.LogWarning("Hit: " + gameObject.name);
        GameManage.Instance.SetScore();
    }
}
