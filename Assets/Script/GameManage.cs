using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using GameObject = UnityEngine.GameObject;

public class GameManage : MonoBehaviour
{
    public static GameManage Instance;
    
    //Level Settings
    private float RemainingTime;
    private int RevealSpots;
    private int CurrentSpots;

    [Header("Ui elements")]
    public TextMeshProUGUI TimerText;
    public TextMeshProUGUI RevealSpotsText;
    public Slider TimerBar;

    private Vector2 mousePos;
    
    [Header("Sprite assets")]
    public GameObject CorrectSpot;
    public GameObject WrongSpot;

    [Header("Ui Panels")]
    public GameObject GameoverPanel;
    public GameObject WinningPanel;

    public GameObject[] Stars;
    private int StarResult;

    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode loadSceneMode)
    {
        RemainingTime = Archive.Instance.levelsStats[SceneManager.GetActiveScene().buildIndex - 1].LimitTime;
    }

    private void Awake()
    {
        Instance = this;
        
        GameObject[] AllSpot = GameObject.FindGameObjectsWithTag("Spot");

        foreach (GameObject spot in AllSpot)
        {
            CurrentSpots ++;
        }
        CurrentSpots /= 2;
        RevealSpots = 0;
    }

    private void Update()
    {
        TimerText.text = RemainingTime.ToString(format: "F0");
        RevealSpotsText.text = (RevealSpots.ToString() + "/" + CurrentSpots.ToString());

        if (RemainingTime > 0 && RevealSpots != CurrentSpots)
        {
            RemainingTime -= Time.deltaTime;
            TimerBar.value = RemainingTime/120;
        }

        if (RemainingTime <= 0) GameOver();

        
        
        /*if (Input.GetKeyDown(KeyCode.W))
        {
            if (RemainingTime > 0 && RemainingTime < 30) Winning(1);
            else if (RemainingTime >= 30 && RemainingTime < 60) Winning(2);
            else if (RemainingTime >= 60 && RemainingTime < 120) Winning(3);
        }*/
    }

    public void Click(InputAction.CallbackContext context)
    {
        if (RemainingTime > 0) if (context.started) ClickAction();
    }
    public void MousePosition(InputAction.CallbackContext context)
    {
        Vector2 screenPos = context.ReadValue<Vector2>();
        mousePos = Camera.main.ScreenToWorldPoint(screenPos);
    }
    private void ClickAction()
    {
        RaycastHit2D spotHit = Physics2D.Raycast(mousePos, Vector2.zero);
        RaycastHit2D picHit = Physics2D.Raycast(mousePos, Vector2.zero);

        if (spotHit.collider != null && spotHit.collider.CompareTag("Spot"))
        {
            Debug.LogWarning("Found Spot: " + spotHit.collider.gameObject.name);
            if (!spotHit.collider.gameObject.GetComponent<Spot>().IsHasBeenSpot)
            {
                spotHit.collider.gameObject.GetComponent<Spot>().HitTheSpot(spotHit.collider.gameObject);
                AudiosManager.Instance.PlaySound("Correct");
            }
            else return;
        }
        else if (picHit.collider != null && picHit.collider.CompareTag("Picture"))
        {
            GameObject canvasRef = GameObject.Find("Canvas");
            GameObject crossRef = Instantiate(WrongSpot, mousePos, Quaternion.identity);
            AudiosManager.Instance.PlaySound("Wrong");

            crossRef.transform.SetParent(canvasRef.transform);
            crossRef.transform.localScale = new Vector3(0.7f, 0.7f, 0.7f);
            RemainingTime -= 5f;
        }
    }

    public void SetScore()
    {
        RevealSpots++;
        if (RevealSpots >= CurrentSpots)
        {
            if (RemainingTime > 0 && RemainingTime < 30) Winning(1);
            else if (RemainingTime >= 30 && RemainingTime < 60) Winning(2);
            else if (RemainingTime >= 60 && RemainingTime < 120) Winning(3);
        }
    }

    private void Winning(int StarCondition)
    {
        WinningPanel.SetActive(true);
        ClearMarker();
        AudiosManager.Instance.BGM.Stop();
        AudiosManager.Instance.PlaySound("Winning");
        
        switch (StarCondition)
        {
            case 1:
                StarResult = 0;
                break;
            case 2:
                StarResult = 1;
                break;
            case 3:
                StarResult = 2;
                break;
        }

        for (int i = 0; i <= StarResult; i++)
        {
            Stars[i].gameObject.SetActive(true);
        }
    }

    private bool DoOnce = true;
    private void GameOver()
    {
        RemainingTime = 0;
        GameoverPanel.SetActive(true);
        ClearMarker();
        if (DoOnce)
        {
            DoOnce = false;
            AudiosManager.Instance.BGM.Stop();
            AudiosManager.Instance.PlaySound("Gameover");
        }
    }

    private void ClearMarker()
    {
        GameObject[] DisplayedCircle = GameObject.FindGameObjectsWithTag("Marker");
        foreach (GameObject ActiveCircle in DisplayedCircle)
        {
            Destroy(ActiveCircle);
        }
        DisplayedCircle = new GameObject[0];
    }
}
