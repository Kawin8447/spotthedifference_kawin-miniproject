using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "Data", menuName = "LevelStats")]

public class LevelStats : ScriptableObject
{
    [Header("Max Time limit of level")]
    public float LimitTime;
    
    [Header("Star Conditions")]
    public float Star1;
    public float Star2;
    public float Star3;
}
