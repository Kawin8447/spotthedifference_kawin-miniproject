using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonManagement : MonoBehaviour
{
    public void Call_Scene(int index)
    {
        SceneManager.LoadScene(index);
    }

    public void Call_TogglePanel(GameObject gameObject)
    {
        StartCoroutine(DelayForTogglePanel(gameObject));
        AudiosManager.Instance.PlaySound("Button");
    }
    private IEnumerator DelayForTogglePanel(GameObject Target)
    {
        yield return new WaitForSeconds(0.2f);
        Target.SetActive(!Target.activeSelf);
    }
    
    public void ReloadScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    
    public void ToggleAudioSetting()
    {
        GameObject AudioSettingPanel = GameObject.Find("---AudioSettingPanel---");
        GameObject AudioPanel = AudioSettingPanel.transform.Find("SettingPanel").gameObject;
        if (AudioPanel != null)
        {
            AudiosManager.Instance.PlaySound("Button");
            AudioPanel.SetActive(!AudioPanel.activeSelf);
        }
    }
    public void Pause(GameObject gameObject)
    {
        AudiosManager.Instance.PlaySound("Button");
        gameObject.SetActive(!gameObject.activeSelf);
        Time.timeScale = 0;
    }
    public void UnPause(GameObject gameObject)
    {
        AudiosManager.Instance.PlaySound("Button");
        gameObject.SetActive(!gameObject.activeSelf);
        Time.timeScale = 1;
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
